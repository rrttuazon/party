#=========
#Variables
#=========

CFLAGS = -std=c99 -Wall -Wextra -pedantic -O0
LFLAGS = 
CC = gcc

GPROF = -pg
GCOV = -fprofile-arcs -ftest-coverage

#=========
#Rules
#=========

#Type "make gprof" to compile game.c to gprof executable

gprof: game.c
	$(CC) $(CFLAGS) $(GPROF) game.c -o gprof


#Type "make gcov" to compile game.c to gcov executable

gcov: game.c
	$(CC) $(CFLAGS) $(GCOV) game.c -o gcov



#Type "make clean" to run this rule
#It removes all files ending with .o .gcov .gcda .gcno, as well as
#gprof, gcov, and gmon.out if they exist

clean:
	rm -rf *.o gprof gcov *.gcov *.gcda *.gcno gmon.out
